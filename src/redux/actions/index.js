import constants from '../constants';

export const fetchTodos = (filter) => ({
  type: constants.FETCH_TODOS_REQUEST,
  filter,
});

export const addTodo = (text) => ({
  type: constants.ADD_TODO_REQUEST,
  text
});

export const toggleTodo = (id, completed) => ({
  type: constants.TOGGLE_TODO_REQUEST,
  id,
  completed
});

export const startTodoEditing = (id) => ({
  type: constants.EDIT_TODO_START,
  id
});

export const stopTodoEditing = () => ({
  type: constants.EDIT_TODO_STOP,
});

export const saveTodo = (id, text) => ({
  type: constants.SAVE_TODO_REQUEST,
  id,
  text
});

export const deleteTodo = (id) => ({
  type: constants.DELETE_TODO_REQUEST,
  id
});