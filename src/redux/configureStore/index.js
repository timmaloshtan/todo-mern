import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { createLogger } from 'redux-logger';

import todos from '../reducers';
import rootSaga from '../sagas';

const sagaMiddleware = createSagaMiddleware()

const configureStore = () => {
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  const middlewares = [sagaMiddleware];
  
  // if (process.env.NODE_ENV !== 'production') {
  //   middlewares.push(createLogger());
  // }
  
  const store = createStore(
    todos,
    composeEnhancers(
      applyMiddleware(...middlewares)
    )
  );

  sagaMiddleware.run(rootSaga);

  return store;
}

export default configureStore;