import constants from '../constants';

const byId = (state = {}, action) => {
  if (action.response) {
    return {
      ...state,
      ...action.response.entities.todos,
    }
  }
  switch (action.type) {
    case constants.DELETE_TODO_SUCCESS:
      const newState = { ...state };
      delete newState[action.id];
      return newState;
    default:
      return state;
  }
};

export default byId;

export const getTodo = (state, id) => state[id];