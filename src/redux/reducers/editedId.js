import constants from '../constants';

const editedId = (state = null, action) => {
  switch (action.type) {
    case constants.EDIT_TODO_START:
     return action.id;
    case constants.EDIT_TODO_STOP:
     return null;
    default:
      return state;
  }
};

export default editedId;

export const getEditedId = (state) => state;