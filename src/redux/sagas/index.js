import { put, takeEvery, all, call } from 'redux-saga/effects';
import { normalize } from 'normalizr';
import * as schema from './schema';

import * as api from '../../api';
import constants from '../constants';

// watcher Saga for fetch todos
export function* watchFetchTodos() {
  yield takeEvery(constants.FETCH_TODOS_REQUEST, fetchTodos);
}

// worker Saga for fetch todos
export function* fetchTodos(action) {
  try {
    const response = yield call(api.fetchTodos, action.filter);
    yield put({
      type: constants.FETCH_TODOS_SUCCESS,
      filter: action.filter,
      response: normalize(response, schema.arrayOfTodos)
    });
  } catch (error) {
    yield put({
      type: constants.FETCH_TODOS_FAILURE,
      filter: action.filter,
      message: error.message || 'Something went wrong.'
    })
  }
}

// watcher Saga for add todo
export function* watchAddTodo() {
  yield takeEvery(constants.ADD_TODO_REQUEST, addTodo);
}

// worker Saga for add todo
export function* addTodo(action) {
    const response = yield call(api.addTodo, action.text);
    yield put({
      type: constants.ADD_TODO_SUCCESS,
      response: normalize(response, schema.todo)
    });
}

// watcher Saga for toggle todo
export function* watchToggleTodo() {
  yield takeEvery(constants.TOGGLE_TODO_REQUEST, toggleTodo);
}

export function* toggleTodo(action) {
  const response = yield call(api.toggleTodo, action.id, action.completed);
  yield put({
    type: constants.TOGGLE_TODO_SUCCESS,
    response: normalize(response, schema.todo)
  });
}

// watcher Saga for save todo
export function* watchSaveTodo() {
  yield takeEvery(constants.SAVE_TODO_REQUEST, saveTodo);
}

export function* saveTodo(action) {
  yield put({
    type: constants.EDIT_TODO_STOP
  });

  const response = yield call(api.saveTodo, action.id, action.text);
  yield put({
    type: constants.SAVE_TODO_SUCCESS,
    response: normalize(response, schema.todo)
  });
}

// watcher Saga for delete todo
export function* watchDeleteTodo() {
  yield takeEvery(constants.DELETE_TODO_REQUEST, deleteTodo);
}

function* deleteTodo(action) {
  yield put({
    type: constants.EDIT_TODO_STOP
  });

  const response = yield call(api.deleteTodo, action.id);
  yield put({
    type: constants.DELETE_TODO_SUCCESS,
    id: response
  });
}

// root Saga
export default function* rootSaga() {
  yield all([
    watchFetchTodos(),
    watchAddTodo(),
    watchToggleTodo(),
    watchSaveTodo(),
    watchDeleteTodo()
  ]);
}
