import React from 'react';

const FetchError = ({ message, onRetry }) => (
  <div>
    <p>
      Could not fetch new todos. {message}
      <button onClick={onRetry}>Retry</button>
    </p>
  </div>
);

export default FetchError; 