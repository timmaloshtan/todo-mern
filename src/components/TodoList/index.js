import React from 'react';

import Todo from '../Todo';

const TodoList = ({
  todos,
  editedId,
  toggleTodo,
  startTodoEditing,
  saveTodo,
  stopTodoEditing,
  deleteTodo,
}) => (
  <ul>
    {todos.map(todo => 
      <Todo
        key={todo.id}
        {...todo}
        isEdited={todo.id === editedId}
        onClick={(e) => toggleTodo(todo.id, e.target.checked)}
        onDoubleClick={() => startTodoEditing(todo.id)}
        onCancel={() => stopTodoEditing()}
        onSave={saveTodo.bind(null, todo.id)}
        onDelete={() => deleteTodo(todo.id)}
      />
    )}
  </ul>
);

export default TodoList;