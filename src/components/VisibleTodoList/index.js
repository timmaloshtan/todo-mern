import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import * as actions from '../../redux/actions';
import { getVisibleTodos, getIsFetching, getErrorMessage, getEditedId } from '../../redux/reducers';
import TodoList from '../TodoList';
import FetchError from '../FetchError';
import loadingIndicator from '../../assets/loadIndicator.gif';

class VisibleTodoList extends Component {
  componentDidMount() {
    this.fetchData()
    this.unlisten = this.props.history.listen(
      (e) => this.props.stopTodoEditing()
    );
  }

  componentWillUnmount() {
    this.unlisten();
  }

  componentDidUpdate(prevProps) {
    if (this.props.filter !== prevProps.filter) {
      this.fetchData();
    }
  }

  fetchData() {
    const { filter, fetchTodos, isFetching } = this.props;
    if (isFetching) {
      return;
    }
    fetchTodos(filter);
  }

  render() {
    const { 
      todos,
      isFetching,
      errorMessage,
    } = this.props;
    if (isFetching && !todos.length) {
      return <img
                src={loadingIndicator}
                style={{height: '60px', width: '80px'}}
                alt="Loading..."
              />
    }
    
    return (
      <div>
        <TodoList {...this.props} />
        {
          errorMessage ?
          <FetchError
            message={errorMessage}
            onRetry={() => this.fetchData()}
          /> :
          null
        }
      </div>
    );
  }
}

const mapStateToProps = (state, { match: { params } }) => {
  const filter = params.filter || 'all';
  return {
    todos: getVisibleTodos(state, filter),
    isFetching: getIsFetching(state, filter),
    errorMessage: getErrorMessage(state, filter),
    editedId: getEditedId(state),
    filter
  }
};

export default withRouter(connect(
  mapStateToProps,
  actions
)(VisibleTodoList));