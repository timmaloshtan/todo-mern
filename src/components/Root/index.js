import React from 'react';
import { Provider } from 'react-redux';
import { Router, Route } from 'react-router';
import createBrowserHistory from 'history/createBrowserHistory';
import App from '../App';

const newHistory = createBrowserHistory();

const Root = ({ store }) => (
  <Provider store={store}>
    <Router history={newHistory}>
      <Route path='/:filter?' component={App} />
    </Router>
  </Provider>
);

export default Root;