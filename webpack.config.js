const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');

const production = process.env.NODE_ENV === 'production';

module.exports = {
  entry: ['babel-polyfill', './src/index.js'],
  output: {
    path: path.resolve(__dirname, "public"),
    filename: "bundle.js"
  },
  watch: !production,
  watchOptions: {
    aggregateTimeout: 100,
    poll: true
  },
  plugins: [
    new CleanWebpackPlugin(['public'], { exclude: ['index.html', 'favicon.ico']})
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["es2015", "react", "stage-2"],
            plugins: ["transform-object-rest-spread"]
          }
        }
      },
      {
        test: /\.(pdf|jpg|png|gif|svg|ico)$/,
        use: [
            {
                loader: 'file-loader'
            },
        ]
    }
    ]
  },
}